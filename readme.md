# R3C Utils #

Biblioteca de utilidades para outros pacotes

# HTTP Utils #
Funções de cash para metodos GET e POST

```
#!php
use R3C\Utils\HTTPUtils;

//Mostra o valor de um request GET. Caso ele já tenha sido feito anteriormente, o valor retornado será o contido na cash, por padrão, a cache é de 60 minutos.
echo HTTPUtils::getCache($url);

//O segundo parâmetro é opcional. Determina o tempo da cache.
echo HTTPUtils::getCache($url, 10);

//Cache do método POST
$inputs = array(
	"id" => "01",
	"nome" => "Fulano",
);
echo HTTPUtils::postCache($url, $inputs)

```

# ThumbnailHelper #
Função para receber apenas o source de uma imagem

```
#!php
use R3C/Utils/ThumbnailHelper;

echo ThumbnailHelper::getImageSrc('src="image/random.jpeg"'); //image/random.jpeg

```
# Slug #
Gera uma slug de algum texto
```
#!php
use R3C\Utilz\Slug;

echo Slug::generate('Ola Mundo'); //ola-mundo
```

## Namespace ##
R3C\HTTPUtils

# Dependências #
Este pacote não possui nenhuma dependência.

# Classes #

## HTTPUtils ##
Classe com funções que criam uma cache para requisições http, tanto para metodos GET quanto para POST.

### Funções ###
* cache
	* Parâmetro
		* String - url da requisição http em que se deseja realizar a cache.
		* integer - tempo, em minutos, para que a cache seja atualizada.
		* String ('post' || 'get') - metodo disparado pela requisição http.
	* Responsável por gerar a cache da requisição http
* get
	* Parâmetro
		* String - url da requisição http em que se deseja realizar a cache.
	* Cria um arquivo de cache para metodos GET, ou recebe da cash os dados armazenados
* getCache
	* Parâmetro
		* String - url da requisição http em que se deseja realizar a cache.
		* integer (opciona) - tempo até que a cache seja atualizada novamente.
	* Retorna o valor de uma requisição GET na url, se a última atualização da cache foi feita a mais de 60 minutos, a cache é atualizada. Caso queira o tempo de atualização da cache maior ou menor, basta utilizar o segundo parâmetro da função, mostrando o tempo de atualização da cache em minutos.
* post
	* Parâmetro
		* String - url da requisição http em que se deseja realizar a cache.
	* Cria um arquivo de cache para metodos POST, pi recebe da cash os dados armazenados
* postCache
	* Parâmetro
		* String - url da requisição http em que se deseja realizar a cache.
	* Gera um cache para o metodo POST realizado na requisição HTTP da url.

## ThumbnailHelper ##
Retorna apenas a source de uma imagem.

## Slug ##
Gera um slug de um texto, caracteres todos em minusculo, sem caracteres especiais, e com traço (-) no lugar de espaços.

##Pluralize ##
Coloca a string no plural, seguindo normal da lingua portuguesa (BR).
```
#!php
use R3C\Utils\Pluralize;

$a = 'copo';
echo Pluralize::plural($a);		//copos
```
