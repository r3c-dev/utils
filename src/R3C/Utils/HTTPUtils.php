<?php
namespace R3C\Utils;

class HTTPUtils
{
	private static function cache($url, $minutes = 60, $method = 'post') {
		//Removes /, http:// and https:// from the filename
		$filename = './cache/' . str_replace('/', '', str_replace('http://', '', str_replace('https://', '', $url))) . '.txt';

		if (file_exists($filename) && filemtime($filename) + (60 * $minutes) > time()) {
			$fp = fopen($filename, 'r');

			if (filesize($filename) > 0) {
				return fread($fp, filesize($filename));
			} else {
				return '';
			}
		} else {
			if ($method == 'get') {
				$content = self::get($url);	
			} else if ($method == 'post') {
				$content = self::post($url);	
			}

			$fp = fopen($filename, 'w');
			fwrite($fp, $content);
			fclose($fp);

			return $content;		
		}
	}

	public static function get($url) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '60');
		$content = trim(curl_exec($ch));
		curl_close($ch);

		return $content;
	}

	public static function getCache($url, $minutes = 60) {
		return self::cache($url, $minutes, 'get');
	}

	public static function post($url, $fields = array()) {
		$ch = curl_init();

		if (count($fields) > 0) {
			//url-ify the data for the POST
		    $fields_string = '';
			
			foreach ($campos as $key => $value) { 
				$fields_string .= $key . '=' . urlencode($value) . '&';
			}

			rtrim($fields_string, '&');

			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		} else {
			curl_setopt($ch, CURLOPT_POST, false);
		}

		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '60');
		$content = trim(curl_exec($ch));
		curl_close($ch);

		return $content;
	}

	public static function postCache($url, $minutes = 60) {
		self::cache($url, $minutes, 'post');
	}
}

?>