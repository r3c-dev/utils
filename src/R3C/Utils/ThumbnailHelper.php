<?php
namespace R3C\Utils;

class ThumbnailHelper
{
	public static function getImageSrc($thumbnail)
	{
		return (preg_match('~\bsrc="([^"]++)"~', $thumbnail, $matches)) ? $matches[1] : '';
	}
}