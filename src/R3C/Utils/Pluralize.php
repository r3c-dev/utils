<?php
namespace R3C\Utils;

class Pluralize
{
	public static function plural($singular)
	{
		$singular = strtolower(trim($singular));
		$len = strlen($singular);
		$lastChar = substr($singular, $len - 1, 1);
		$lastTwoChars = substr($singular, $len - 2, 2);

		//lápis = lápis
		if ($lastChar == 's') {
			return $singular;
		}

		if ($lastTwoChars == 'ão') {
			//macarrão - macarrões
			if ($len > 3) {
				return substr($singular, 0, $len - 2) . 'ões';
			}

			//pão - pães
			//TODO - Tratar mão - mãos
			return substr($singular, 0, $len - 2) . 'ães';
		}

		//para versão sem acento
		if ($lastTwoChars == 'ao') {
			if ($len > 3) {
				return substr($singular, 0, $len - 2) . 'oes';
			}

			return substr($singular, 0, $len - 2) . 'aes';
		}

		if ($lastTwoChars == 'ãe') {
			//mãe - mães
			return $singular . 's';
		}

		//para versão sem acento
		if ($lastTwoChars == 'ae') {
			return $singular . 's';
		}

		if ($lastChar == 'm') {
			//bom - bons, som - sons
			return substr($singular, 0, $len - 1) . 'ns';	
		}

		if ($lastChar == 'l') {
			//legal - legais
			return substr($singular, 0, $len - 1) . 'is';	
		}

		if ($lastChar == 'z' || $lastChar == 'r') {
			//paz - pazes, olhar - olhares
			return $singular . 'es';		
		}

		//mouse - mouses, xícara, xícaras, telefone - telefones, mesa - mesas
		return $singular . 's';	
	}
}